import React from "react";
import "../styles/tailwind.css";
import TypeWriter from "./TypeWriter";
import govImage from "../image/gov.png";
import { Button } from "antd";
import DataInput from "./DataInput";
import Background from "./Background";

class Main extends React.Component {
	state = {
		reload: true,
		open: false
	};

	handleDrawerOpen = () => {
		this.setState({ open: true });
	};

	handleDrawerClose = () => {
		this.setState({ open: false });
	};

	setReload = callback =>
		this.setState(prevState => ({ reload: !prevState.reload }), callback);

	onReload = () => {
		this.setReload(this.setReload);
	};
	render() {
		const { reload, open } = this.state;
		const { classes } = this.props;
		return (
			<div>
				<DataInput onClose={this.handleDrawerClose} visible={open} />

				<div className="my-12 mx-auto w-5/6 h-400 items-center shadow relative p-4">
					<Background logo={govImage} title="जानकी गाउँपालिका" />
					{reload && <TypeWriter />}
				</div>
				<div className="px-4">
					<Button
						className="ml-1"
						icon="setting"
						type="primary"
						onClick={this.handleDrawerOpen}
					>
						Open Settings
					</Button>
					<Button
						className="mx-2"
						type="primary"
						icon="reload"
						onClick={this.onReload}
					>
						Reload
					</Button>
					<small>Please reload manually after changing settings</small>
				</div>
			</div>
		);
	}
}

export default Main;
