import React from "react";
import Typist from "./typist/";
import { Zoom, withStyles } from "@material-ui/core";
import * as Actions from "../store/actions/form/form.actions.js";
import { useDispatch, useSelector } from "react-redux";
import "../styles/style.css";
const data = {
	config: {
		avgTypingDelay: 70,
		content: {
			fontType: "preeti", //[unicode, preeti]
			listType: "none", // [none,checkbox,bullets]
			fontSize: "2xl" //[xs,sm,md,lg,xl,2xl,5xl]
		},
		cursor: {
			show: false,
			blink: true,
			element: "|",
			hideWhenDone: false,
			hideWhenDoneDelay: 1000
		}
	},
	title: {
		name: "नक्सा पास", //name of page
		delay: 500, //in ms
		color: "rgb(227,92,0)", //color for name
		type: "underline", //underline or type
		animation: "zoom-in" //animation: ease-in, handwriting
	},
	subTitle: {
		name: "आबस्यक पर्ने कागजातहरु",
		delay: 800, //in ms
		color: "#f44336", //color of text
		type: "box", // [box, underline]
		animation: "ease-in"
	},
	content: [
		{
			key: "nibedan_patra",
			animation: "handwriting",
			// title: "१) निबेदन पत्र",
			title: "!_ lga]bg kq",
			color: "#2196f3"
		},
		{
			key: "nibedan_patra",
			animation: "handwriting",
			// title: "२) घर बनाउन प्रस्ताब गरेको नक्सा",
			title: "@_ 3/ agfpg k|:tfa u/]sf] gS;f ",
			color: "#2196f3"
		},
		{
			key: "nibedan_patra",
			animation: "handwriting",
			// title: "३) नागरिकता, लालपुर्जा, ट्रेस नक्सा, ब्लु प्रिन्ट नक्साको फोटोकपि १/१ प्रति",
			title:
				"#_ gful/stf, nfnk'hf{, 6«]; gS;f, An' lk|G6 gS;fsf] kmf]6f]slk !÷! k|lt",
			color: "#2196f3"
		}
	]
};

function TypeWrite({ classes }) {
	const dispatch = useDispatch();
	const { avgTypingDelay, showCursor, showBlink, font, fontSize } = useSelector(
		({ form }) => form.config
	);
	const { titleDelay, pageTitle } = useSelector(({ form }) => form.title);
	const { pageSubtitle, subTitledelay } = useSelector(
		({ form }) => form.subTitle
	);

	const content = useSelector(({ form }) => form.content);
	const maxValue = 500;
	return (
		<div className="p-4">
			{data.title && (
				<Zoom in={true} style={{ transitionDelay: `${titleDelay}ms` }}>
					<div
						className=" p-0 m-0 text-center "
						style={{
							color: data.title.color,
							fontSize: 50,
							fontWeight: 900,
							textShadow: `1px 0 ${data.title.color}`,
							letterSpacing: 1
						}}
					>
						<span
							// className={classes.underlineAnimation}
							className={`underline_animate ${font}`}
						>
							{pageTitle}
						</span>
					</div>
				</Zoom>
			)}
			{pageSubtitle && (
				<Zoom
					in={true}
					style={{ transitionDelay: `${titleDelay + subTitledelay}ms` }}
				>
					<div
						className="mt-28 text-center mb-8"
						style={{
							color: data.subTitle.color,
							fontSize: 25,
							fontWeight: 500,
							textShadow: `1px 0 ${data.subTitle.color}`,
							letterSpacing: 1
						}}
					>
						<span className="box_animate p-2 ">{pageSubtitle}</span>
					</div>
				</Zoom>
			)}
			<Typist
				avgTypingDelay={maxValue - avgTypingDelay}
				cursor={{
					element: "|",
					hideWhenDone: false,
					hideWhenDoneDelay: 1000,
					show: showCursor,
					blink: showBlink
				}}
			>
				<p> </p>
				<Typist.Delay ms={2500} />
				<ul className="list-none">
					{content.map(each => (
						<li
							key={each.title}
							className={`${font} mt-3 font-bold ${fontSize}`}
							style={{
								color: each.color ? each.color : "#2196f3"
							}}
						>
							{each.title}
						</li>
					))}
				</ul>
			</Typist>
		</div>
	);
}

// const styles = theme => ({
// 	"@keyframes underline": {
// 		to: { width: "100%" }
// 	},
// 	underlineAnimation: {
// 		position: "relative",
// 		paddingBottom: 4,
// 		marginBottom: 5,
// 		"&:after": {
// 			position: "absolute",
// 			content: '"',
// 			bottom: "-1px",
// 			left: 0,
// 			height: 5,
// 			width: "0%",
// 			backgroundColor: "rgb(227, 92, 92)",
// 			// animation: "$underline 1s ease-in-out 1s";
// 			animationName: "$underline",
// 			animationDuration: "1s",
// 			animationTimingFunction: "linear",
// 			animationIterationCount: "infinite",
// 			animationFillMode: "forwards"
// 		}
// 	}
// });

// export default withStyles(styles)(TypeWrite);
export default TypeWrite;
