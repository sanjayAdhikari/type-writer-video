import React from "react";
import {
	Drawer,
	Form,
	Button,
	Col,
	Row,
	Input,
	Select,
	DatePicker,
	Icon,
	Collapse,
	Slider,
	InputNumber,
	Checkbox,
	Table
} from "antd";
import * as Actions from "../store/actions/form/form.actions.js";
import { useDispatch, useSelector } from "react-redux";
const { Panel } = Collapse;
const { Option, OptGroup } = Select;
function SpeedControl({ title, ...props }) {
	return (
		<div className="flex flex-row items-center">
			<span className="mr-2">{title}</span>
			<div className="flex-1">
				<Slider {...props} />
			</div>
		</div>
	);
}

function Content() {
	const dispatch = useDispatch();
	const { content, maxContentIndex } = useSelector(({ form }) => form);

	const columns = [
		{
			title: "content",
			dataIndex: "sn",
			key: "content",

			render: each => (
				<Input
					name="title"
					value={content && content[each] && content[each].title}
					onChange={event =>
						dispatch(Actions.setContent(each, "title", event.target.value))
					}
				/>
			)
		},
		{
			title: "Action",
			key: "action",
			dataIndex: "sn",
			render: index => (
				<Button type="error" onClick={() => dispatch(Actions.removeRow(index))}>
					<Icon style={{ color: "red" }} type="minus" />
					Remove
				</Button>
			)
		}
	];

	const dataSource = [...Array(maxContentIndex).keys()].map(sn => ({
		sn
	}));
	return (
		<div className="w-full">
			<Table
				title={() => (
					<Button type="dashed" onClick={() => dispatch(Actions.addRow())}>
						<Icon type="plus" />
						Add
					</Button>
				)}
				dataSource={dataSource}
				columns={columns}
				pagination={false}
			/>
		</div>
	);
}
function DataInput({ form: { getFieldDecorator }, onClose, visible }) {
	const dispatch = useDispatch();
	const { avgTypingDelay, projectName } = useSelector(
		({ form }) => form.config
	);
	const { titleDelay, pageTitle } = useSelector(({ form }) => form.title);
	const { subTitledelay, pageSubtitle } = useSelector(
		({ form }) => form.subTitle
	);

	return (
		<Drawer
			title="Change to content"
			width={720}
			onClose={onClose}
			visible={visible}
			bodyStyle={{ paddingBottom: 80 }}
		>
			<Form layout="vertical">
				<Collapse bordered={false} defaultActiveKey={["2", "3"]}>
					<Panel header="Config of the System" key="1">
						<SpeedControl
							title="Typing Speed:"
							onChange={value =>
								dispatch(Actions.setConfig("avgTypingDelay", value))
							}
							value={avgTypingDelay}
							min={0}
							max={500}
						/>
						<SpeedControl
							title="Title Delay:"
							onChange={value =>
								dispatch(Actions.setTitle("titleDelay", value))
							}
							value={titleDelay}
							min={300}
							max={3000}
						/>
						<SpeedControl
							title="Sub Title Delay:"
							onChange={value =>
								dispatch(Actions.setSubTitle("subTitledelay", value))
							}
							value={subTitledelay}
							min={0}
							max={3000}
						/>
						<div className="flex flex-row items-center mt-8">
							<div className="flex flex-row items-center mt-8 w-1/2">
								<span className="mr-2">Choose Font:</span>
								<Select
									defaultValue="ganesh"
									style={{ width: 200 }}
									onChange={value => dispatch(Actions.setConfig("font", value))}
								>
									<OptGroup label="Nepali Font">
										<Option value="preeti">Preeti</Option>
										<Option value="ganesh">Ganesh</Option>
									</OptGroup>
									<OptGroup label="Normal">
										<Option value="unicode">Unicode</Option>
										<Option value="sansSerif">Sans Serif</Option>
									</OptGroup>
								</Select>
							</div>
							<div className="flex flex-row items-center mt-8 w-1/2">
								<span className="mr-2">Choose Font Size:</span>
								<Select
									defaultValue="text-2xl"
									style={{ width: 200 }}
									onChange={value =>
										dispatch(Actions.setConfig("fontSize", value))
									}
								>
									<Option value="text-lg">smallest</Option>
									<Option value="text-xl">smaller</Option>
									<Option value="text-2xl">small</Option>
									<Option value="text-3xl">medium</Option>
									<Option value="text-4xl">big</Option>
									<Option value="text-5xl">bigger</Option>
									<Option value="text-6xl">Biggest</Option>
								</Select>
							</div>
						</div>
						<div className="flex flex-row items-center mt-8">
							<div className="mt-8 w-full">
								<Checkbox
									onChange={e =>
										dispatch(Actions.setConfig("showCursor", e.target.checked))
									}
								>
									Show Cursor
								</Checkbox>
							</div>
							<div className="mt-8 w-full">
								<Checkbox
									onChange={e =>
										dispatch(Actions.setConfig("showBlink", e.target.checked))
									}
								>
									Show Blink
								</Checkbox>
							</div>
						</div>
					</Panel>
					<Panel header="Heading" key="2">
						<div className="flex flex-row items-center mt-8 w-full">
							<span className="mr-2">Project Name:</span>
							<Input
								value={projectName}
								size="large"
								placeholder="जानकी गाउँपालिका"
								onChange={e =>
									dispatch(Actions.setConfig("projectName", e.target.value))
								}
							/>
						</div>
						<div className="flex flex-row items-center mt-8 w-full">
							<span className="mr-2 inline-block">Tiltle:</span>
							<Input
								value={pageTitle}
								size="large"
								placeholder="नक्सा पास"
								onChange={e =>
									dispatch(Actions.setTitle("pageTitle", e.target.value))
								}
							/>
						</div>

						<div className="flex flex-row items-center mt-8 w-full">
							<span className="mr-2 inline-block">Sub Title:</span>
							<Input
								value={pageSubtitle}
								size="large"
								placeholder="आबस्यक पर्ने कागजातहरु"
								onChange={e =>
									dispatch(Actions.setSubTitle("pageSubtitle", e.target.value))
								}
							/>
						</div>
					</Panel>
					<Panel header="Content" key="3">
						<Content />
					</Panel>
				</Collapse>
			</Form>
			<div
				style={{
					position: "absolute",
					right: 0,
					bottom: 0,
					width: "100%",
					borderTop: "1px solid #e9e9e9",
					padding: "10px 16px",
					background: "#fff",
					textAlign: "right"
				}}
			>
				<Button
					type="danger"
					onClick={() => {
						dispatch(Actions.resetSettings());
					}}
					style={{ marginRight: 8 }}
				>
					Reset Settings
				</Button>
				<Button
					type="danger"
					onClick={() => {
						dispatch(Actions.resetData());
					}}
					style={{ marginRight: 8 }}
				>
					Reset Data
				</Button>
				<Button
					type="danger"
					onClick={() => {
						dispatch(Actions.resetEverything());
					}}
					style={{ marginRight: 8 }}
				>
					Reset Everything
				</Button>
			</div>
		</Drawer>
	);
}
export default Form.create()(DataInput);
