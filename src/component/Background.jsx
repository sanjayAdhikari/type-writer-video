import React from "react";
import { useSelector } from "react-redux";

export default function Background({ logo }) {
	const projectName = useSelector(({ form }) => form.config.projectName);
	return (
		<div
			className="absolute top-0 bottom-0 left-0 right-0 flex flex-col justify-center items-center"
			style={{
				opacity: 0.1
			}}
		>
			<img src={logo} alt="gov logo" width="200px" />
			<p className="m-0 mt-8 text-4xl font-black">{projectName}</p>
		</div>
	);
}
