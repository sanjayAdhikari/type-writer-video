import api from "axios";
import store from "app/store";
import { logoutUser } from "app/auth/store/actions";
import JWTService from "../../services/jwtService/";
import { startLoader, stopLoader } from "../actions/util";

api.interceptors.request.use(
	function(config) {
		const token = JWTService.getAccessToken();
		const licenseCode = JWTService.getAccessToken();
		if (token) {
			config.headers.Authorization = token;
			config.headers.licenseCode = licenseCode;
		} else {
			store.dispatch(logoutUser());
		}

		store.dispatch(startLoader());
		return config;
	},
	function(err) {
		store.dispatch(stopLoader());
		return Promise.reject(err);
	}
);

api.interceptors.response.use(
	function(config) {
		store.dispatch(stopLoader());
		return config;
	},
	function(err) {
		if (err.response && err.response.status) {
			if (err.response.status === 401) {
				store.dispatch(logoutUser());
			}
		}
		store.dispatch(stopLoader());
		return Promise.reject(err);
	}
);

const url = {
	index: "/",
	currentUserDetail: "users/current",
	login: "/login",
	product: "/api/products",
	translate: "/api/translate",
	office: "/api/office",
	// here
	firebase: "https://fcm.googleapis.com/fcm/send",
	application: "application",
	notification: "notification"
};

export default {
	user: {
		currentUserDetail: function() {
			return api.get(`${url.api}/${url.currentUserDetail}/`);
		}
	},

	notification: {
		send: function(authorization, jsonData) {
			const headers = {
				"Content-Type": "application/json",
				Authorization: `key=${authorization}`
			};
			// return instance.post(url.firebase, jsonData, { headers });

			return fetch(url.firebase, {
				method: "POST", // or 'PUT'
				body: JSON.stringify(jsonData), // data can be `string` or {object}!
				headers: headers
			}).then(res => res.json());
		},
		list: function() {
			return api.get(`${url.api}/${url.notification}/`);
		},

		get: function(id) {
			return api.get(`${url.api}/${url.notification}/id/${id}`);
		},

		save: function(data) {
			return api.post(`${url.api}/${url.notification}/`, data);
		},

		del: function(ids) {
			return api.post(`${url.api}/${url.notification}/many`, { ids });
		}
	},
	management:{
		product: {
			brand: {
				list: function() {
					return api.get(`${url.product}/brand/`);
				},
				get: function(productId) {
					return api.get(`${url.product}/brand/id/${productId}`);
				},
				save: function(data) {
					return api.post(`${url.product}/brand/`, data);
				},
				edit: function(data) {
					return api.post(`${url.product}/brand/edit`, data);
				},
				delete: function(productId) {
					return api.delete(`${url.product}/brand/id/${productId}`);
				},
				deleteMany: function(ids) {
					return api.post(`${url.product}/brand/delete`, { ids });
				}
			},
			unit: {
				list: function() {
					return api.get(`${url.product}/unit/`);
				},
				get: function(productId) {
					return api.get(`${url.product}/unit/id/${productId}`);
				},
				save: function(data) {
					return api.post(`${url.product}/unit/`, data);
				},
				edit: function(data) {
					return api.post(`${url.product}/unit/edit`, data);
				},
				delete: function(productId) {
					return api.delete(`${url.product}/unit/id/${productId}`);
				},
				deleteMany: function(ids) {
					return api.post(`${url.product}/unit/delete`, { ids });
				}
			},
			type: {
				list: function() {
					return api.get(`${url.product}/type/`);
				},
				get: function(productId) {
					return api.get(`${url.product}/type/id/${productId}`);
				},
				save: function(data) {
					return api.post(`${url.product}/type/`, data);
				},
				edit: function(data) {
					return api.post(`${url.product}/type/edit`, data);
				},
				delete: function(productId) {
					return api.delete(`${url.product}/type/id/${productId}`);
				},
				deleteMany: function(ids) {
					return api.post(`${url.product}/type/delete`, { ids });
				}
			},
			condition: {
				list: function() {
					return api.get(`${url.product}/condition/`);
				},
				get: function(productId) {
					return api.get(`${url.product}/condition/id/${productId}`);
				},
				save: function(data) {
					return api.post(`${url.product}/condition/`, data);
				},
				edit: function(data) {
					return api.post(`${url.product}/condition/edit`, data);
				},
				delete: function(productId) {
					return api.delete(`${url.product}/condition/id/${productId}`);
				},
				deleteMany: function(ids) {
					return api.post(`${url.product}/condition/delete`, { ids });
				}
			},
			source: {
				list: function() {
					return api.get(`${url.product}/source/`);
				},
				get: function(productId) {
					return api.get(`${url.product}/source/id/${productId}`);
				},
				save: function(data) {
					return api.post(`${url.product}/source/`, data);
				},
				edit: function(data) {
					return api.post(`${url.product}/source/edit`, data);
				},
				delete: function(productId) {
					return api.delete(`${url.product}/source/id/${productId}`);
				},
				deleteMany: function(ids) {
					return api.post(`${url.product}/source/delete`, { ids });
				}
			},
			tax: {
				list: function() {
					return api.get(`${url.product}/tax/`);
				},
				get: function(productId) {
					return api.get(`${url.product}/tax/id/${productId}`);
				},
				save: function(data) {
					return api.post(`${url.product}/tax/`, data);
				},
				edit: function(data) {
					return api.post(`${url.product}/tax/edit`, data);
				},
				delete: function(productId) {
					return api.delete(`${url.product}/tax/id/${productId}`);
				},
				deleteMany: function(ids) {
					return api.post(`${url.product}/tax/delete`, { ids });
				}
			},
			scope: {
				list: function() {
					return api.get(`${url.product}/scope/`);
				},
				get: function(productId) {
					return api.get(`${url.product}/scope/id/${productId}`);
				},
				save: function(data) {
					return api.post(`${url.product}/scope/`, data);
				},
				edit: function(data) {
					return api.post(`${url.product}/scope/edit`, data);
				},
				delete: function(productId) {
					return api.delete(`${url.product}/scope/id/${productId}`);
				},
				deleteMany: function(ids) {
					return api.post(`${url.product}/scope/delete`, { ids });
				}
			},
			purchaseType: {
				list: function() {
					return api.get(`${url.product}/purchaseType/`);
				},
				get: function(productId) {
					return api.get(`${url.product}/purchaseType/id/${productId}`);
				},
				save: function(data) {
					return api.post(`${url.product}/purchaseType/`, data);
				},
				edit: function(data) {
					return api.post(`${url.product}/purchaseType/edit`, data);
				},
				delete: function(productId) {
					return api.delete(`${url.product}/purchaseType/id/${productId}`);
				},
				deleteMany: function(ids) {
					return api.post(`${url.product}/purchaseType/delete`, { ids });
				}
			},
			budgetType: {
				list: function() {
					return api.get(`${url.product}/budgetType/`);
				},
				get: function(productId) {
					return api.get(`${url.product}/budgetType/id/${productId}`);
				},
				save: function(data) {
					return api.post(`${url.product}/budgetType/`, data);
				},
				edit: function(data) {
					return api.post(`${url.product}/budgetType/edit`, data);
				},
				delete: function(productId) {
					return api.delete(`${url.product}/budgetType/id/${productId}`);
				},
				deleteMany: function(ids) {
					return api.post(`${url.product}/budgetType/delete`, { ids });
				}
			}
		},
		office: {
			designation: {
				list: function() {
				return api.get(`${url.office}/designation/`);
				},
				get: function(systemId) {
					return api.get(`${url.office}/designation/id/${systemId}`);
				},
				save: function(data) {
					return api.post(`${url.office}/designation/`, data);
				},
				edit: function(data) {
					return api.post(`${url.office}/designation/edit`, data);
				},
				delete: function(systemId) {
					return api.delete(`${url.office}/designation/id/${systemId}`);
				},
				deleteMany: function(ids) {
					return api.post(`${url.office}/designation/delete`, { ids });
				}
			},
		}
	},
	system: {
		translate: {
			system: {
				list: function() {
				return api.get(`${url.translate}/systemTranslate/`);
				},
				get: function(systemId) {
					return api.get(`${url.translate}/systemTranslate/id/${systemId}`);
				},
				save: function(data) {
					return api.post(`${url.translate}/systemTranslate/`, data);
				},
				edit: function(data) {
					return api.post(`${url.translate}/systemTranslate/edit`, data);
				},
				delete: function(systemId) {
					return api.delete(`${url.translate}/systemTranslate/id/${systemId}`);
				},
				deleteMany: function(ids) {
					return api.post(`${url.translate}/systemTranslate/delete`, { ids });
				}
			},
			client: {
				list: function() {
				return api.get(`${url.translate}/clientTranslate/`);
				},
				get: function(clientId) {
					return api.get(`${url.translate}/clientTranslate/id/${clientId}`);
				},
				save: function(data) {
					return api.post(`${url.translate}/clientTranslate/`, data);
				},
				edit: function(data) {
					return api.post(`${url.translate}/clientTranslate/edit`, data);
				},
				delete: function(clientId) {
					return api.delete(`${url.translate}/clientTranslate/id/${clientId}`);
				},
				deleteMany: function(ids) {
					return api.post(`${url.translate}/clientTranslate/delete`, { ids });
				}
			}
		}
	}
};
