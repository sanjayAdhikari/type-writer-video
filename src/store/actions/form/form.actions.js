export const RESET_DATA = "RESET_DATA";
export const RESET_SETINGS = "RESET_SETINGS";
export const RESET_EVERYTHING = "RESET_EVERYTHING";
export const SET_CONFIG = "SET_CONFIG";
export const SET_TITLE = "SET_TITLE";
export const SET_SUBTITLE = "SET_SUBTITLE";
export const SET_CONTENT = "SET_CONTENT";
export const RELOAD_UI = "RELOAD_UI";
export const REMOVE_ROW = "REMOVE_ROW";
export const ADD_ROW = "ADD_ROW";

export const resetData = () => dispatch => {
	dispatch({
		type: RESET_DATA
	});
};

export const resetSettings = () => dispatch => {
	dispatch({
		type: RESET_SETINGS
	});
};

export const resetEverything = () => dispatch => {
	dispatch({
		type: RESET_EVERYTHING
	});
};

export const setConfig = (name, value) => dispatch => {
	dispatch({
		type: SET_CONFIG,
		name: name,
		value: value
	});
};

export const setTitle = (name, value) => dispatch => {
	dispatch({
		type: SET_TITLE,
		name: name,
		value: value
	});
};

export const setSubTitle = (name, value) => dispatch => {
	dispatch({
		type: SET_SUBTITLE,
		name: name,
		value: value
	});
};

export const setContent = (index, name, value) => dispatch => {
	dispatch({
		type: SET_CONTENT,
		name,
		value,
		index
	});
};

export const addRow = () => dispatch => {
	dispatch({
		type: ADD_ROW
	});
};

export const removeRow = index => dispatch => {
	dispatch({
		type: REMOVE_ROW,
		index
	});
};
