import _ from "lodash";
export const RESET_DATA = "RESET_DATA";
export const RESET_SETINGS = "RESET_SETINGS";
export const RESET_EVERYTHING = "RESET_EVERYTHING";
export const SET_CONFIG = "SET_CONFIG";
export const SET_TITLE = "SET_TITLE";
export const SET_SUBTITLE = "SET_SUBTITLE";
export const SET_CONTENT = "SET_CONTENT";
export const RELOAD_UI = "RELOAD_UI";
export const REMOVE_ROW = "REMOVE_ROW";
export const ADD_ROW = "ADD_ROW";

const initialState = {
	reload: false,
	maxContentIndex: 1,
	config: {
		avgTypingDelay: 70,
		font: "ganesh", //[unicode, preeti]
		fontSize: "text-2xl", //[xs,sm,md,lg,xl,2xl,5xl]
		showCursor: true,
		showBlink: true,
		projectName: "जानकी गाउँपालिका"
	},
	title: {
		pageTitle: "नक्सा पास", //name of page
		titleDelay: 500, //in ms
		color: "rgb(227,92,0)", //color for name
		type: "underline", //underline or type
		animation: "zoom-in" //animation: ease-in, handwriting
	},
	subTitle: {
		pageSubtitle: "आबस्यक पर्ने कागजातहरु",
		subTitledelay: 800, //in ms
		color: "#f44336", //color of text
		type: "box", // [box, underline]
		animation: "ease-in"
	},
	content: [
		{
			title: "please add content"
		}
	]
};

export default (state = initialState, action) => {
	switch (action.type) {
		case RELOAD_UI:
			return {
				...state,
				reload: !state.reload
			};
		case ADD_ROW:
			return {
				...state,
				maxContentIndex: state.maxContentIndex + 1
			};
		case REMOVE_ROW:
			var contentRow = state.content;
			contentRow.splice(action.index, 1);
			return {
				...state,
				content: contentRow,
				maxContentIndex: state.maxContentIndex - 1
			};

		case RESET_DATA:
			return {
				...state,
				content: state.content
			};

		case RESET_SETINGS:
			return {
				...state,
				config: state.config
			};

		case RESET_EVERYTHING:
			return initialState;

		case SET_CONFIG:
			return {
				...state,
				config: {
					...state.config,
					[action.name]: action.value
				}
			};
		case SET_TITLE:
			return {
				...state,
				title: {
					...state.title,
					[action.name]: action.value
				}
			};
		case SET_SUBTITLE:
			return {
				...state,
				subTitle: {
					...state.subTitle,
					[action.name]: action.value
				}
			};
		case SET_CONTENT:
			var content = state.content;
			if (content[action.index] === undefined) content[action.index] = {};
			const thisData = content[action.index];
			const data = { ...thisData, [action.name]: action.value };
			content.splice(action.index, 1, data);
			return {
				...state,
				content
			};
		default:
			return state;
	}
};
