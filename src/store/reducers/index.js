import { combineReducers } from "redux";
import formReducer from "./form/form.reducer";
const createReducer = asyncReducers =>
	combineReducers({
		form: formReducer,
		...asyncReducers
	});

export default createReducer;
