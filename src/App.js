import React from "react";
import "./styles/tailwind.css";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Main from "./component/Main";
import Provider from "react-redux/es/components/Provider";
import store from "./store";
import "antd/dist/antd.css";
function App() {
	return (
		<div>
			<Provider store={store}>
				<Router>
					<Switch>
						<Route exact path="/" component={Main} />
					</Switch>
				</Router>
			</Provider>
		</div>
	);
}

export default App;
